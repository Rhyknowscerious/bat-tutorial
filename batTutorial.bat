@ECHO OFF

SET _title=Batch Script Tips

:_main
TITLE %_title%

ECHO. & ECHO Welcome to my Bat Script Tips book. & ECHO.
CALL :_chapterSelectionMenu
GOTO :_quitMenu

:_chapterSelectionMenu
	SETLOCAL
		ECHO Which chapter do you want to read? & ECHO.
		SET /A _choice=1
		ECHO 1) The @ Symbol
		ECHO 2) The ECHO Command
		ECHO 3) Getting Help
		ECHO 4) Working With Output
		ECHO. & ECHO Enter a number.
		SET /P _choice=(Or just push ENTER to start at Chapter1): 
		ECHO.
		IF %_choice% LSS 1 (
			ECHO Invalid selection: %_choice% & ECHO.
			GOTO :_chapterSelectionMenu
		)
		IF %_choice% GTR 4 (
			ECHO Invalid selection: %_choice% & ECHO.
			GOTO :_chapterSelectionMenu
		)
		IF %_choice% EQU 1 CALL :_chapter1
		IF %_choice% EQU 2 CALL :_chapter2
		IF %_choice% EQU 3 CALL :_chapter3
		IF %_choice% EQU 4 CALL :_chapter4
	ENDLOCAL
EXIT /B 0

:_quitMenu
	CHOICE /C YN /M "Would you like to continue reading"
	IF ERRORLEVEL 2 GOTO :EOF
	IF ERRORLEVEL 1 (
		CLS
		GOTO :_main
	)
EXIT /B 0

:_delay
	@PING 127.0.0.1 -n %~1 > NUL
@EXIT /B 0

:_chapter1
	CLS
	TITLE %_title%: Ch1 - The @ Symbol
	ECHO.
	ECHO In batch scripts, the @ sign just tells the console not to repeat a ^
command when you execute it. & ECHO.

	PAUSE & CLS
	ECHO.
	ECHO For example, here's what "ECHO HI"  looks like: & ECHO.
	CALL :_delay 2
	
	ECHO OUTPUT START
	ECHO ------------
	CALL :_delay 2
	
	@ECHO ON
	ECHO HI
	@CALL :_delay 2
	
	@ECHO -----------
	@ECHO OUTPUT STOP
	@ECHO OFF
	ECHO.
	CALL :_delay 3
	
	ECHO Now here's what "@ECHO HI" looks like: & ECHO.
	CALL :_delay 2
	
	ECHO OUTPUT START
	ECHO ------------
	CALL :_delay 2
	
	@ECHO ON
	@ECHO HI
	@CALL :_delay 2
	
	@ECHO -----------
	@ECHO OUTPUT STOP
	@ECHO OFF
	CALL :_delay 3
	
	ECHO.
	
	ECHO Notice that "ECHO HI" produces 3 lines of output and "@ECHO HI" only ^
produces 1 line? & ECHO.
	CALL :_delay 2

EXIT /B 0

:_chapter2
	CLS
	TITLE %_title%: Ch2 - The ECHO Command
	ECHO.
	ECHO The ECHO command has options ON and OFF that set the console to ^
repeat commands or not - kind of like the @ sign in Chapter 1. & ECHO.
	CALL :_delay 2
	
	ECHO The difference is that if you use ECHO OFF, then you don't have to ^
use the @ symbol for every single command. You can just toggle command-^
echoing on or off. & ECHO.
	CALL :_delay 2

	ECHO Push ENTER to try it out.
	PAUSE > NUL & ECHO.
	
	START "Try ECHO OFF and ECHO ON" CMD /K "ECHO Execute the command ECHO OFF. You'll lose your prompt but that's ok. & ECHO Then execute WHOAMI. & ECHO Then execute @ECHO ON
	
	PAUSE & ECHO.
	
	ECHO To enter a blank line to the output, use ECHO.
	CALL :_delay 2
	
	ECHO Don't forget to use the period. There are 5 characters: E C H O . & ECHO.
	CALL :_delay 2
EXIT /B 0

:_chapter3
	CLS
	TITLE %_title%: Ch3 - Getting Help
	ECHO.
	
	ECHO Pretty much anything and everything that you can do on the command line will have instructions if you type /? after it. & ECHO.
	CALL :_delay 2
	
	ECHO For example, here's what happens when you want to learn more about the TIMEOUT command: & ECHO.
	CALL :_delay 2
	
	PAUSE & ECHO.
	
	START "Example: TIMEOUT /?" CMD /C "ECHO TIMEOUT /? & TIMEOUT /? & ECHO. & PAUSE"
	
	ECHO Ok, your turn. Push ENTER to try it out.
	PAUSE > NUL & ECHO.
	
	START "Practice getting help" CMD /K "ECHO Execute the command HELP /? & ECHO Then execute the command HELP alone. & ECHO Now try any command you're curious about and remember to use /?"
EXIT /B 0

:_chapter4
	CLS
	TITLE %_title%: Ch4 - Working With Output
	ECHO.
	
	ECHO There are several ways to work with and control the output with CMD.exe and batch scripts. & ECHO.
	
	PAUSE & ECHO.
	
	TITLE Working With Output: Input and Output Operators
	
	CLS
	ECHO.
	ECHO There are two simple input and output operators you can use to either insert data into a command from a file or transfer one command's output to another command's input. & ECHO.
	CALL :_delay 2
	
	ECHO Pipe Operator: ^|
	ECHO Input operator: ^< & ECHO.
	CALL :_delay 2
	
	ECHO Use the pipe to take one command's output as the next command's input: DIR ^| CLIP. & ECHO.
	CALL :_delay 2
	
	ECHO Use the input operator to use a file's contents as input to a command: CLIP ^< file.txt & ECHO.
	CALL :_delay 2
	
	ECHO If you don't know what DIR or CLIP do, use the /? like you read in Chapter 3: DIR /? or CLIP /? & ECHO.
	CALL :_delay 2
	
	ECHO Push ENTER to do that now...
	
	PAUSE > NUL & ECHO.
	
	START "Get help about DIR and CLIP" CMD /K "ECHO Find out more about DIR and CLIP & ECHO Enter the command DIR /? & ECHO Then enter the command CLIP /?"
	
	PAUSE & ECHO.
	
	TITLE Working With Output: CLIP
	
	CLS
	ECHO.
	ECHO You can redirect a command's output to the clipboard with the CLIP command. & ECHO.
	CALL :_delay 2
	
	ECHO Push ENTER to see its help page again (you could also use the CLIP /? command) and try it out.
	PAUSE > NUL
	
	START "CLIP /? Output" CMD /K "CLIP /? | MORE & ECHO. & ECHO Go ahead and try to use the CLIP command now..."
	
	ECHO. & PAUSE & ECHO.
	
	TITLE Working With Output: Mouse Selection
	
	CLS
	ECHO.
	ECHO You can also use the mouse to copy and paste to and from the command line. & ECHO.
	CALL :_delay 2
	
	ECHO You can right click anywhere in the console and click "Mark." & ECHO.
	CALL :_delay 2
	
	ECHO Then you can highlight text. If you right click after making a selection, you'll automatically copy that selection to the clipboard. & ECHO.
	CALL :_delay 2
	
	ECHO Push ENTER to give it a try:
	PAUSE > NUL & ECHO.
	
	START "Mouse Selection" CMD /K "ECHO Right click and select Mark to start highlighting text."
	
	TITLE Working With Output: MORE
	
	PAUSE & ECHO.
	
	CLS
	ECHO.
	ECHO Next, the MORE command is known as what is called a "pager". It takes long outut that goes off the screen and pauses output so you don't miss anything. & ECHO.
	CALL :_delay 2
	
	ECHO With MORE, you can scroll through output line-by-line by pushing ENTER or page-by-page by pushing SPACEBAR. & ECHO.
	CALL :_delay 2
	
	ECHO If you don't want to scroll all the way through the output, you can push Q to quit before reaching the end. & ECHO.
	CALL :_delay 2
	
	ECHO Push ENTER to give it a try:
	PAUSE > NUL & ECHO.
	
	START "The MORE Command" CMD /K "ECHO Execute the command HELP ^| MORE"
	
	:: FILE I/O CHAPTER
	:: output to file
	:: append to file
	:: output errors
	:: web link for more information
	:: SET /P "=text" < NUL hack
	
	:: the escape character
	:: one command multiple lines ^
	:: multiple commands one line &
	
	:: SET command
	:: Variables
	:: For loop
	:: Variable scope
	:: Delayed expansion
	
	:: Subroutines
	:: IF command
	:: Conditional operators && ||
	
	:: Simple and complex menues
	
EXIT /B 0

















